class UsuarioWeb { /*Clase padre */
	constructor(user,password,nombre,apellido) {
		this.user=user;
		this.password=password;
		this.nombre=nombre;
		this.apellido=apellido;
	}
	
}

class UsuarioGestor extends UsuarioWeb { /*Clase hija 1 */
	constructor(user,password,nombre,apellido) {
		super(user,password,nombre,apellido); //hereda de UsuarioWeb
		this.user=user;
		this.password=password;
		this.nombre=nombre;
		this.apellido=apellido;
	}
	
}

class UsuarioCliente extends UsuarioWeb { /*Clase hija 2 */
	constructor(user,password,nombre,apellido) {
		super(user,password,nombre,apellido); //hereda de UsuarioWeb
		this.user=user;
		this.password=password;
		this.nombre=nombre;
		this.apellido=apellido;
	}
	
}

var attempt = 3; // Numero de intentos que tiene para logearse.
// Funcion validar cuando hacemos click en el boton del formulario.
function validate(){
var username = document.getElementById("username").value;
var password = document.getElementById("password").value;
if ( username == "gestObj1" && password == "gestObj#123"){
    alert ("Login successfully");
    window.location = "inicio.html"; // Redirige a la pagina del inicio.
    return false;
}else if(username == "clientObj1" && password == "clientObj#123"){
    alert ("Login successfully");
    window.location = "inicio.html"; // Redirige a la pgina del inicio
    return false;
}
else{
attempt --;// Decrementing by one.
alert("You have left "+attempt+" attempt;");
// Disabling fields after 3 attempts.
if( attempt == 0){
document.getElementById("username").disabled = true;
document.getElementById("password").disabled = true;
document.getElementById("submit").disabled = true;
return false;
}
}
}